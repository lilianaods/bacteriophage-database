HistoryId: NCID_1_68832734_130.14.22.33_5555_1554895873_135664558_0MetA0_S_HStore
QueryKey: 21
ReleaseType: RefSeq
FileType: genomic.fna.gz
Flat: true

Query title: Select 2 document(s)
    
Search results count: 2
Filtered out 1 entries that do not have the requested ReleaseType, or are suppressed.
Entries to download: 1

genomic.fna.gz files in archive: 1
Total size (bytes): 43141
Total time: 302 milliseconds
